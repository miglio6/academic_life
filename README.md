# AcademicLife

AcademicLife è una piattaforma web pensata per intercettare le esigenze degli studenti nel corso del loro percorso universitario. Con tale progetto si ha l’ambizione di soddisfare alcuni di quei bisogni che si possono generalmente definire comuni ad ogni studente.

La piattaforma è costruita a partire da una conoscenza fornita dalla base di dati gestita dall’Università di Ferrara. AcademicLife è progettata per essere un prodotto software che agisce in maniera integrata con il contesto universitario ferrarese.

## Panoramica delle funzionalità
Ciascuno studente, accedendo ad AcademicLife (idealmente utilizzando le credenziali unife), può usufruire dei seguenti servizi:

* Visione degli insegnamenti presenti nel suo piano di studi. Per ciascun corso è presente la media dei voti e le recensioni degli altri studenti.

* A ciascun corso di studio è associata una bacheca. Gli studenti che stanno frequentando quel corso la possono utilizzare per pubblicare post, commentare, creare gruppi e pubblicare documenti;

* In ciascun gruppo sono ammesse tre tipologie di utenti: il creatore, che gode di privilegi da amministratore (è il solo soggetto col potere di eliminare il gruppo e aggiungere/rimuovere membri); il membro, ovvero uno studente che ha ricevuto l’invito e che l’ha accettato; l’invitato, cioè colui che ha ricevuto l’invito ma che deve ancora decidere se accettarlo o declinarlo;

* In ciascun gruppo è possibile schedulare degli eventi. Un evento è concepito come un modo per organizzare i momenti di studio tra un gruppo affine di persone;

* Una sezione di notifiche per aggiornare lo studente in merito a eventuali inviti in gruppi, creazioni di eventi o commenti a post in cui è coinvolto.

## Team di sviluppo

* Mattia Fogli

* Andrea Fortini

* Matteo Miglioli
